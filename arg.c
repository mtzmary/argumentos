
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
    FILE *fe, *fs;
    char linea[50];
    char c;
    int dentroPalabra = 0, numeroPalabras = 0;
    int i;
    unsigned char buffer[2048]; // Buffer de 2 Kbytes
    int bytesLeidos;

    if(argc != 3) {
       printf("Usar: copia <fichero_origen> <fichero_destino>\n");
       return 1;
    }

    // Abrir el fichero de entrada en lectura y binario
    fe = fopen(argv[1], "rb");
    if(!fe) {
       printf("El fichero %s no existe o no puede ser abierto.\n", argv[1]);
       return 1;
    }
    // Crear o sobreescribir el fichero de salida en binario
    fs = fopen(argv[2], "wb");
    if(!fs) {
       printf("El fichero %s no puede ser creado.\n", argv[2]);
       fclose(fe);
       return 1;
    }


    // Bucle de copia:
while((bytesLeidos = fread(buffer, 1, 2048, fe)))
       fwrite(buffer, 1, bytesLeidos, fs);

printf("Desde el programa -->%s, file_src-->%s, file_dst -->%s\n", argv[0], argv[1], argv[2]);		
	for(i = 0; i < argc; i++){
		printf("argumento[%d] = %s\n", i, argv[i]);
	}
  while ((c = getc(fe)) != EOF)
  {
    if (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')))
	{
	  if (dentroPalabra==0)
	  {
	    numeroPalabras += 1;
		dentroPalabra = 1;
	  }
	}
	else if (dentroPalabra == 1)
      dentroPalabra = 0;
  }
  printf("\n Total de elementos _file_src [%d] \n", numeroPalabras);
 
//MOSTRAR PALABRAS
printf ("\n <<<<< Palabras en la lista enlazada >>>>>> \n");

fe = fopen(argv[1], "r");
while(fgets(linea,100,(FILE*)fe)){
printf ("PALABRA = %s \n",linea);
}


//ORDENAR

    // Cerrar ficheros:
    fclose(fe);
    fclose(fs);
    return 0;
}
